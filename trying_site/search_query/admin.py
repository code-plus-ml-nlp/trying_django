from django.contrib import admin

# Register your models here.
from .models import Search_Query

admin.site.register(Search_Query)
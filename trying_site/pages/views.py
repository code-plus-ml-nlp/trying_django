from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.



# def home page
def home_view(request, *args, **kwargs):
    print(request.user.username)
    if request.user.is_superuser:
        return HttpResponse("<h1>This is the code plus search engine <br> What are you going to edit Prince?</h1>")
    return HttpResponse("<h1>This is the code plus search engine</h1>")